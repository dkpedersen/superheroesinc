﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SuperHeroesInc.UI.Models
{
    public class ContactViewModel
    {
        /*
         There are 5 basic requirements to send email through a web application.
         1. Input form
         2. View Model to represent the email object
         3. HttpGet to display the email form 
         4. HttpPost to recieve the email 
         5. Delivery confirmation

        We also need to configure the email server in the websites host. (Smarter ASP for us)

        Go to contact form and see what info is required
         */
        [Required(ErrorMessage = "* Please enter your name")]
        public string Name
        {
            get; set;
        }

        [Required(ErrorMessage = "* Email is Required")]
        [EmailAddress(ErrorMessage = "Please enter a valid Email adress")]
        public string Email
        {
            get; set;
        }

        [Required(ErrorMessage = "* Please enter a subject")]
        public string Subject
        {
            get; set;
        }

        [Required(ErrorMessage = "* Please enter your message")]
        public string Message
        {
            get; set;
        }

    }
}