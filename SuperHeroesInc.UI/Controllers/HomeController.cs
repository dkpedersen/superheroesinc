﻿using SuperHeroesInc.UI.Models;
using System;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Web.Mvc;

namespace SuperHeroesInc.UI.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        //[Authorize]
        public ActionResult About()
        {

            return View();
        }

        [HttpGet]
        public ActionResult ContactConfirmation()
        {

            return View();
        }

        [HttpGet]
        public ActionResult Contact()
        {

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact(ContactViewModel contact)
        {




            //We are creating a what we are going to recieve in out inbox from the user
            //create the body for the email. It is a string that we are sending to the recipient(the letter)
            string body = string.Format("Name: {0}<br/>Email: {1}<br/>Subject: {2}<br>Message:<br/>{3}",
                contact.Name, contact.Email, contact.Subject, contact.Message);


            //create and configure the Mail Message object(the envelope)
            MailMessage msg = new MailMessage(
                //from adress (This must be a email on your hosting account)
                "postmaster@danethedev.com", //return adress
                "pedersen.dane@outlook.com", //to adress
                contact.Subject, //the letter that was written
                body //the content
                );
            msg.IsBodyHtml = true; //telling the mail service to format the body as html so it doesn't just appear to your email as a weird string.


            //Create and configure the SMTP client(who is going to be handling the sending of the email)(post office)
            SmtpClient client = new SmtpClient("mail.danethedev.com");//The email hosting service
            client.Credentials = new NetworkCredential("postmaster@danethedev.com", "Nobody0815!");



            //Send the user to a confirmation view
            if (ModelState.IsValid)
            {
                using (client)
                {
                    try
                    {
                        client.Send(msg);
                        return RedirectToAction("ContactConfirmation");
                        //return ViewBag.Message("Thank you for contacting us"); //Return a confirmation if the email actually sends
                    }
                    catch
                    {
                        ViewBag.ErrorMessage = "There was a problem sending your email. Please try again later.";
                        return View();//Return an error message if the email does not send.
                    }
                }              

            }//endifmodel
            return View();
        }


       


        [HttpGet]
        public ActionResult Archive()
        {

            return View();
        }


        [HttpGet]
        public ActionResult Single()
        {

            return View();
        }
        [HttpGet]
        public ActionResult Courses()
        {

            return View();
        }



    }
}
